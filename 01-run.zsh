#!/bin/zsh
set -e

## You can change these #
declare KEYBOARD_LAYOUT="br-abnt2"
declare TIMEZONE="America/Sao_Paulo"
declare CHROOT_LANG="pt_BR.UTF-8"
declare CHROOT_EDITOR="neovim"
declare HOSTNAME="archlinux"
declare CHROOT_USERNAME="khalil"
declare PASSWORD="archlinux"

BASE_PKGS=(
    kate
    elisa
    vlc
    obs-studio
);
##----------------------#


declare -r LEGACY_BIOS="LEGACY_BIOS"
declare -r UEFI_BIOS="UEFI_BIOS"
declare BOOT_MDOE
declare -a BLOCK_DEVICES
declare TARGET_BLOCK_DEVICE
declare -r BTRFS_MOUNT_OPTIONS="noatime,compress=zstd:1"

setup-enviroment() {
    echo "[*] Configuring enviroment"
    loadkeys $KEYBOARD_LAYOUT
    echo "[*] Using $KEYBOARD_LAYOUT"
    echo -n "[*] Testing internet connection..."
    ping -c1 archlinux.org 1>/dev/null
    echo "done."
    echo -n "[*] Enabling NTP..."
    timedatectl set-ntp true
    CURR_TIME=$(date)
    timedatectl set-timezone $TIMEZONE
    echo "current time $CURR_TIME. done"
}

detect-boot-mode() {
    if [ -d /sys/firmware/efi/efivars ]; then
        BOOT_MODE=$UEFI_BIOS;
    else
        BOOT_MODE=$LEGACY_BIOS;
    fi
    echo "[*] Detected $BOOT_MODE";
}

find-block-devices() {
    echo "[*] Available block devices:";
    BLOCK_DEVICES=( $(find /dev/disk/by-id -follow -type b | egrep -v "part|wwn") );
}

get-target-block-dev() {
    find-block-devices
    PS3="[?] Select the target block device: ";
    select BDEV in ${BLOCK_DEVICES}
    do
        TARGET_BLOCK_DEVICE=$BDEV;
        break;
    done;
    echo "[*] Using $TARGET_BLOCK_DEVICE";
}

delete-existing-filesystems() {
    echo -n "[*] Deleting existing partitions...";
    wipefs -qa $TARGET_BLOCK_DEVICE 1>/dev/null;
    blkdiscard -f $TARGET_BLOCK_DEVICE >/dev/null;
    echo "done.";
}

create-esp-partition() {
    echo -n "[*] Partitioning disk (1/2) - Creating ESP partition...";
    parted -s $TARGET_BLOCK_DEVICE -- mklabel gpt mkpart "ESP" fat32 0% 1025MiB
    parted -s $TARGET_BLOCK_DEVICE -- set 1 esp on
    partprobe 2>/dev/null
    mkfs.fat -F32 -n "ESP" $TARGET_BLOCK_DEVICE-part1 1>/dev/null
    echo "done.";
}

create-linuxroot-partition() {
    echo -n "[*] Partitioning disk (2/2) - Creating BtrFS partition...";
    parted -s $TARGET_BLOCK_DEVICE -- mkpart "$HOSTNAME" btrfs 1025MiB 100%;
    partprobe 2>/dev/null
    mkfs.btrfs --data single --metadata dup --checksum crc32c \
        -R free-space-tree                                    \
        -L "$HOSTNAME"  --force --quiet                       \
        $TARGET_BLOCK_DEVICE-part2;
    echo "done.";
}

create-btrfs-subvols() {
    echo -n "[*] Creating subvolumes..."
    mount -o $BTRFS_MOUNT_OPTIONS $TARGET_BLOCK_DEVICE-part2 /mnt
    btrfs -q subvolume create /mnt/@
    btrfs -q subvolume create /mnt/@home
    btrfs -q subvolume create /mnt/@srv
    btrfs -q subvolume create /mnt/@var-log
    btrfs -q subvolume create /mnt/@snapshots
    mkdir -p /mnt/@/{home,boot,srv,var/log,.snapshots}
    btrfs filesystem sync /mnt
    umount /mnt
    echo "done."
}

mount-chroot-destination() {
    echo -n "[*] Mounting subvolumes..."
    mount -o $BTRFS_MOUNT_OPTIONS+",subvol=/@"          $TARGET_BLOCK_DEVICE-part2  /mnt
    mount -o $BTRFS_MOUNT_OPTIONS+",subvol=/@home"      $TARGET_BLOCK_DEVICE-part2  /mnt/home
    mount -o $BTRFS_MOUNT_OPTIONS+",subvol=/@srv"       $TARGET_BLOCK_DEVICE-part2  /mnt/srv
    mount -o $BTRFS_MOUNT_OPTIONS+",subvol=/@var-log"   $TARGET_BLOCK_DEVICE-part2  /mnt/var/log
    mount -o $BTRFS_MOUNT_OPTIONS+",subvol=/@snapshots" $TARGET_BLOCK_DEVICE-part2  /mnt/.snapshots
    echo "done."
    echo -n "[*] Mounting ESP/Boot partition..."
    mount $TARGET_BLOCK_DEVICE-part1 /mnt/boot
    echo "done."
}

partition-target-device() {
    delete-existing-filesystems
    create-esp-partition
    create-linuxroot-partition
    create-btrfs-subvols
    mount-chroot-destination
}

run-pacstrap() {
    echo -n "[*] Refreshing mirrors and sorting by speed..."
    reflector --protocol http --country Brazil --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
    echo "done."

    echo "[*] Downloading base packages into chroot..."
    pacstrap /mnt base linux linux-firmware amd-ucode intel-ucode sudo networkmanager plasma-desktop plasma-nm sddm konsole dolphin discover flatpak packagekit-qt5 $CHROOT_EDITOR "${BASE_PKGS[@]}"
    echo "[*] Finished installing base packages into chroot"

}

generate-fstab() {
    echo -n "[*] Generating fstab..."
    genfstab -U /mnt >> /mnt/etc/fstab
    echo "done."
}
setup-bootloader() {
    echo "[*] Setup bootloader..."
    arch-chroot /mnt bootctl install
    echo "title   Arch Linux"                   >  /mnt/boot/loader/entries/arch.conf
    echo "linux   /vmlinuz-linux"               >> /mnt/boot/loader/entries/arch.conf
    echo "initrd  /intel-ucode.img"             >> /mnt/boot/loader/entries/arch.conf
    echo "initrd  /amd-ucode.img"               >> /mnt/boot/loader/entries/arch.conf
    echo "initrd  /initramfs-linux.img"         >> /mnt/boot/loader/entries/arch.conf
    echo "options root=\"LABEL=$HOSTNAME\" rw rootflags=subvol=/@"  >> /mnt/boot/loader/entries/arch.conf
}

setup-chroot-enviroment() {
    arch-chroot /mnt ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
    arch-chroot /mnt hwclock --systohc

    echo "[*] Configuring and generating locales..."
    echo "$CHROOT_LANG UTF-8" > /mnt/etc/locale.gen
    echo "en_US.UTF-8 UTF-8" >> /mnt/etc/locale.gen
    echo "LANG=$CHROOT_LANG" > /mnt/etc/locale.conf
    echo "KEYMAP=$KEYBOARD_LAYOUT" > /mnt/etc/vconsole.conf
    arch-chroot /mnt locale-gen
    # arch-chroot /mnt localectl set-x11-keymap br pc104
    echo "done."

    echo "[*] Setting up hostname and hosts file"
    echo "$HOSTNAME" > /mnt/etc/hostname
    echo -e "127.0.0.1 localhost\n::1 localhost\n127.0.1.1 $HOSTNAME.localdomain $HOSTNAME"
    local EDITOR_PATH=$(which $CHROOT_EDITOR)
    echo "[*] Setting editor to $CHROOT_EDITOR ($EDITOR_PATH)"
    echo "EDITOR=$CHROOT_EDITOR" >> /mnt/etc/environment

    echo "%wheel      ALL=(ALL) ALL" >> /mnt/etc/sudoers.d/50_allow-wheel

    # Use ZSTD compression by uncommeting the corresponding line in mkinitcpio.conf
    sed -i '/COMPRESSION="zstd"/s/^#//g' /mnt/etc/mkinitcpio.conf

    arch-chroot /mnt systemctl enable NetworkManager
}

create-user() {
    echo "[*] Creating user"
    arch-chroot /mnt useradd -m -G wheel -s /bin/bash $CHROOT_USERNAME
    echo "[*] Setting temporary password and forcing change on login"
    PASSWDSTRING=$CHROOT_USERNAME:$PASSWORD
    arch-chroot /mnt bash -c "echo $PASSWDSTRING | chpasswd"
}

enable-gui() {
    arch-chroot /mnt systemctl enable sddm
}

run-inside-chroot() {
    setup-chroot-enviroment
    setup-bootloader
    create-user
    enable-gui
}

main() {
    setup-enviroment
    detect-boot-mode
    get-target-block-dev
    partition-target-device
    run-pacstrap
    generate-fstab
    run-inside-chroot
}
main
